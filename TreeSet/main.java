import java.io.*;
import java.util.*;
/**
 * @author towerXEON
 */

public class main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String path = "/home/laptop/test.txt";
			// String path = null; // erzeugt Nullpointer Exception
			File file = new File(path);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			String[] splited = null;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");
				/*
				 * Die aktuell ausgelesene Zeile wird hier gesplittet.
				 * Da ich zu faul war sondnerzeichen zu entfernen, die am anfang stehen kommen hier dann am ende Kuriose sachen raus wie zb "(text".
				 * Getrennt wird wird bei \\s+, was einen Freizeichen/" " entspricht.
				*/
				splited = line.split("\\s+");
			}
			// nach der schleife ist die Datei abgearbeitet und alles ist im array

			//get Groesse des Arrays
			int size = splited.length;
			//treeset weil sortiert.
			TreeSet<String> treeSet = new TreeSet<>();
			//array in treeset einfuegen - koennte man theoretisch auch bereits oben machen und sich das Array teilsweise sparen (oder auch ganz)
			for(int i = 0; i<size; i++)
			{
				treeSet.add(splited[i]);
			}
			printtree(treeSet);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void printtree(TreeSet<String> list) {
		for (String str : list) {
			System.out.print(str + "\n");
		}
		System.out.println();
	}
}
