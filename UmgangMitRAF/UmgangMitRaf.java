import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Array;
public class UmgangMitRaf {
	public static void main(String[] args) {
		RandomAccessFile raf = null;
		try{
			//Es wird hier eine Datei HalloWelt.txt erzeugt die im naechsten Schritt beschrieben wird
			raf = new RandomAccessFile("HalloWelt.txt", "rw");
			//Fuer jedes  Argument wird ein String s geschrieben. Von diesem Schreiben wir die Bytes danach wird eine Freizeile eigenfuegt
			 for (String uebergebenesArgument: args) {
				 //Bytecode schreiben vom String
				 raf.write(uebergebenesArgument.getBytes());
				 raf.write('\r');
				 raf.write('\n');
			 }
		}catch(Exception e){//falls error etc
			e.printStackTrace();
		}finally {//Wenn fertig versuche Datei zu schließen
			if(raf!=null){
				try{
					raf.close();
				}catch(IOException ex){
					ex.printStackTrace();
}}}}}
