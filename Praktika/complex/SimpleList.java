
import java.util.*;

public class SimpleList {

    // erstes Element der Liste als "Einstiegspunkt"
    ListElement first;

    // Konstruktor f�r leere Liste
    public SimpleList() {
        first = null;
    }

    // privater Konstruktor f�r Liste mit gegebenem Anfangselement
    private SimpleList(ListElement first) {
        this.first = first;
    }

    // Test auf leere Liste
    public boolean isEmpty() {
        return (first == null);
    }

    // Liste leeren
    public void clear() {
        first = null;
    }

    // Operationen am Anfang der Liste

    // erstes Datenobjekt holen
    public Object getFirst() throws NoSuchElementException {
        if (first == null) {
            throw new NoSuchElementException("Liste leer");
        }
        return first.data;
    }

    // neues erstes Datenobjekt einf�gen
    public void insertFirst(Object data) {
        ListElement e = new ListElement(data, first);
        first = e;
    }

    // bisheriges erstes Datenobjekt l�schen und zur�ckgeben
    public Object deleteFirst() throws NoSuchElementException {
        if (first == null) {
            throw new NoSuchElementException("Liste leer");
        }
        ListElement e = first;
        first = first.next;
        return e.data;
    }

    //* toString() rekursiv
     public String toString() {
         String str = "";
         if (first != null) {
             SimpleList rest = new SimpleList(first.next);
             str = " -> " + first.data + rest.toString();
         }
         return str;
     }

    //*/

    /* alternativ:  toString() iterativ
         public String toString() {
         String str = "";
         ListElement current = first;
         while (current != null) {
             str = str + " -> " + current.data.toString();
             current = current.next;
         }
         return str;
         }
         //*/

     //* equals() iterativ
      public boolean equals(Object o) {
          if (o == this) return true;
          if (! (o instanceof SimpleList)) return false;
          SimpleList l = (SimpleList) o;
          boolean eq = true;
          ListElement current = first;
          ListElement lcurrent = l.first;
          while(eq && (current != null) && lcurrent != null) {
             eq = current.data.equals(lcurrent.data);
             current = current.next;
             lcurrent = lcurrent.next;
          }
          return (eq && (current != null) && lcurrent != null);
      }




     // Testbeispiele
     public static void main(String[] args) {
         int i;
         int n = 7;
         SimpleList l1, l2;
         Object o;

         // Test 1. Konstruktor und insertFirst
         l1 = new SimpleList();
         System.out.println(l1);
         for (i = 0; i < n; i++) {
             l1.insertFirst(new Integer(i));
             System.out.println(l1);
         }
         System.out.println();

         // Test deleteFirst/insertFirst:
         //erste Liste abbauen und in 2. Liste einf�gen
         l2 = new SimpleList();
         System.out.println("Elemente von " + l1 + " umgekehrt in " + l2 +
                            " einf�gen:");
         for (i = 0; i < n; i++) {
             o = l1.deleteFirst();
             System.out.println(o + " entfernt: " + l1);
             l2.insertFirst(o);
             System.out.println(o + " eingef�gt: " + l2);
         }
     }
}
