public class Hofstaedter {
    public static int berechneHofstaedter(int n) {
        int hof;
        if ((n == 0) || (n == 1)) {
            hof = 1;
        } else {
            hof = berechneHofstaedter(n - berechneHofstaedter(n - 1))
                    + berechneHofstaedter(n - berechneHofstaedter(n - 2));
        }
        return hof;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println("Hofstaedter-Folge");
        long hof;
        for (int i = 0; i < n; i++) {
            hof = Hofstaedter.berechneHofstaedter(i);
            System.out.println("Hof(" + i + ") = " + hof);
        }
        System.out.println();
    }

}
