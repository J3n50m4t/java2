public class Fibonacci {
    public static long berechneFibonacci(int n) {
        long f;
        if ((n == 0) || (n == 1)) {
            f = 1;
        } else {
            f = berechneFibonacci(n - 1) + berechneFibonacci(n - 2);
        }
        return f;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        long f;
        System.out.println("Test der Fibonacci-Klasse");
        for (int i = 0; i < n; i = i + 1) {
            f = Fibonacci.berechneFibonacci(i);
            System.out.println("Fib(" + i + ") = " + f);
        }
        System.out.println();
    }

}
