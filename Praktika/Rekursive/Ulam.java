public class Ulam {
    public static int berechneUlamFolge(int start) {
        int laenge;
        System.out.print(start + "  ");
        if (start > 1) {
            if (start % 2 == 0) // start gerade
            {
                laenge = 1 + berechneUlamFolge(start / 2);
            } else // start ungerade
            {
                laenge = 1 + berechneUlamFolge(start * 3 + 1);
            }
        } else {
            laenge = 1;
        }
        return laenge;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println("Ulam-Folgen");

        for (int i = 0; i < n; i++) {
            int laenge = Ulam.berechneUlamFolge(i);
            System.out.println("Laenge = " + laenge);
        }
        System.out.println();
    }

}
