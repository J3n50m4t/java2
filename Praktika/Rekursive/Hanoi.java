public class Hanoi {
    public static long versetzeTurm(int n, String vonpos, String nachpos,
            String hilfpos) {
        long anz = 0;
        if (n == 1) {
            System.out.println("trage Scheibe Nr. " + n + " von " + vonpos
                    + " nach " + nachpos);
            anz = anz + 1;
        } else if (n > 1) {
            anz = versetzeTurm(n - 1, vonpos, hilfpos, nachpos);
            System.out.println("trage Scheibe Nr. " + n + " von " + vonpos
                    + " nach " + nachpos);
            anz = anz + 1;
            anz = anz + versetzeTurm(n - 1, hilfpos, nachpos, vonpos);
        }
        return anz;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out
                .println("Loesung der Tuerme von Hanoi mit " + n + " Scheiben:");
        long anzahl = Hanoi.versetzeTurm(n, " links ", " rechts ", " mitte ");
        System.out.println("Loesung mit " + anzahl + " Schritten");
        System.out.println();
    }

    // kuerzere Variante der Methode
    public static long versetzeTurm1(int n, String vonpos, String nachpos,
            String hilfpos) {
        long anz = 0;
        if (n > 0) // nur fuer n > 0 ist etwas zu tun
        {
            anz = versetzeTurm1(n - 1, vonpos, hilfpos, nachpos);
            System.out.println("trage Scheibe Nr. " + n + " von " + vonpos
                    + " nach " + nachpos);
            anz = anz + 1;
            anz = anz + versetzeTurm1(n - 1, hilfpos, nachpos, vonpos);
        }
        return anz;
    }
}
