public class Ackermann {
    public static int berechneAckermann(int m, int n) {
        int ack;
        if (m == 0) {
            ack = n + 1;
        } else if (n == 0) {
            ack = berechneAckermann(m - 1, 1);
        } else {
            ack = berechneAckermann(m - 1, berechneAckermann(m, n - 1));
        }
        // System.out.println(" A(" + m +"," + n + ")= " + ack);
        return ack;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println("Ackermann");
        long ack;
        for (int h = 0; h <= n; h++) {
            for (int m = 0; m <= h; m++) {
                int k = h - m;
                ack = Ackermann.berechneAckermann(m, k);
                System.out.print("A(" + m + "," + k + ") = " + ack + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
