public class Fakultaet {
    public static long berechneFakultaet(int n) throws IllegalArgumentException {
        if (n > 20) {
            throw new IllegalArgumentException(
                    "Ergebnis zu grooss fuer Datentyp long");
        }
        if (n < 0) {
            throw new IllegalArgumentException(
                    "Fakultaet fuer negative Zahlen nicht definiert");
        }

        long f;
        if (n == 0) {
            f = 1;
        } else {
            f = berechneFakultaet(n - 1) * n;
        }
        return f;
    }

    public static void main(String[] args) {
        int n = 5;
        long f;
        System.out.println("Test der Fakultaet-Klasse");
        f = Fakultaet.berechneFakultaet(n);
        System.out.println(n + "! = " + f);
        System.out.println();
    }
}
