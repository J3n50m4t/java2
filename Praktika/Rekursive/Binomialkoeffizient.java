public class Binomialkoeffizient {
    public static int berechneBinomialkoeffizient(int n, int k) {
        int bin;
        if ((k == 0) || (k == n)) {
            bin = 1;
        } else {
            bin = berechneBinomialkoeffizient(n - 1, k - 1)
                    + berechneBinomialkoeffizient(n - 1, k);
        }
        return bin;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println("Binomialkoeffizienten - Pascalsches Dreieck");
        long bin;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                bin = Binomialkoeffizient.berechneBinomialkoeffizient(i, j);
                System.out.print("  " + bin);
            }
            System.out.println();
        }
        System.out.println();
    }

}
