import java.util.*;
import java.util.Map.Entry;
public class Sammlung {
	public static void main(String[] args) {
		//benutzungHashSet();
		//benutzungTreeSet();
		benutzungHashMap();
	}
	private static void benutzungHashMap() {
		HashMap<String, String> namen = new HashMap<String, String>();
		namen.put("Knut Altroggen", "altroggen@hs-mittweida.de");
		namen.put("Uwe Schneider"	, "uschneid@hs-mittweida.de");
		namen.put("Dirk Pawlaszczyk", "pawlasz@hs-mittweida.de");
		System.out.println(namen.get("Uwe Schneider")); //Wert an Stelle des Schluessel holen
		Iterator iter = namen.keySet().iterator(); //alle Schluessel holen
		while(iter.hasNext()) {
			//Entry<String, String> eintrag = (Entry<String, String>)iter.next();
			System schluessel = (String)iter.next();
			System.out.println(schluessel +" - "+ namen.get(schluessel));
		}}
	private static void benutzungTreeSet() {
		Collection<Double> zahlen = new TreeSet<Double>();
		zahlen.add(5.46);
		zahlen.add(5.45);
		zahlen.add(5.47);
		Iterator iter = zahlen.iterator();
		while(iter.hasNext()){	//sind weitere Elemente vorhanden?
			System.out.println(iter.next());	//next nimmt sich den naechsten Wert
		}
		zahlen.add(5.48);
		zahlen.add(5.49);
		zahlen.add(5.44);
		zahlen.add(5.44);
		zahlen.add(5.43);
		iter=zahlen.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
		zahlen.remove(5.46);
		iter = zahlen.iterator();
		while(iter.hasNext()){//sind weitere Elemente vorhanden?
			System.out.println(iter.next());//next nimmt sich den naechsten Wert
		}}
	private static void benutzungHashSet() {
		Collection<Double> zahlen = new HashSet<Double>();
		zahlen.add(5.46);
		zahlen.add(5.45);
		zahlen.add(5.47);
		Iterator iter = zahlen.iterator();
		while(iter.hasNext()){//sind weitere Elemente vorhanden?
			System.out.println(iter.next());//next nimmt sich den naechsten Wert
		}
		zahlen.add(5.48);
		zahlen.add(5.49);
		zahlen.add(5.44);
		zahlen.add(5.44);
		zahlen.add(5.43);
		iter=zahlen.iterator();
		while(iter.hasNext()){//sind weitere Elemente vorhanden?
			System.out.println(iter.next());//next nimmt sich den naechsten Wert
		}
		zahlen.remove(5.46);
		iter=zahlen.iterator();
		while(iter.hasNext()){//sind weitere Elemente vorhanden?
			System.out.println(iter.next());//next nimmt sich den naechsten Wert
		}}}
