public class sortieren {
	public static void main(String[] args) {
		int anzahlZahlen = 1000000;
		int anzahlWdhlg = 1;
		//int anzahlZahlen;
		int[] zahlen = new int[anzahlZahlen];
		//worst-case-array
		for(int i=0; i< anzahlZahlen;i++){
			zahlen[i] = anzahlZahlen -1 -i;
		}
		long t1, t2; //zeitmessung
		t1 =System.currentTimeMillis();
		for(int i=0; i< anzahlWdhlg;i++);
		mergeSort(zahlen,0,zahlen.length-1);
		t2=System.currentTimeMillis();
		System.out.println("mergesort "+ (t2-t1)/anzahlWdhlg);
		t1 =System.currentTimeMillis();
		for(int i=0; i< anzahlWdhlg;i++);
			quicksort(zahlen,0,zahlen.length-1);
		t2=System.currentTimeMillis();
		System.out.println("quicksort "+ (t2-t1)/anzahlWdhlg);
	}
	private static void quicksort(int[] zahlen, int links, int rechts) {
		int linksindex, rechtsindex;
		int pivot;
		if(links< rechts) {
			linksindex=links;
			rechtsindex=rechts;
			pivot= zahlen[(links+rechts)/2];
			do{
				while(zahlen[linksindex]<pivot){//Suchen nach linken Wert
					linksindex++;
				}
				while(zahlen[rechtsindex]>pivot){//Suchen nach rechten Wert
					rechtsindex--;
				}
				if(linksindex <= rechtsindex){
					int hilf = zahlen[linksindex];
					zahlen[linksindex] = zahlen[rechtsindex];
					zahlen[rechtsindex] = hilf;
					linksindex++;
					rechtsindex--;
				}
			}while(linksindex <=rechtsindex);
			quicksort(zahlen, links, rechtsindex);//Teilarray 1 bearbeitet
			quicksort(zahlen, linksindex, rechts);//Teilarray 2 nearneitet
		}
	}
private static void mergeSort(int[] zahlen, int links, int rechts) {
	if(links<rechts) {
		int mitte = (links+rechts)/2;//pivot element
		mergeSort(zahlen, links, mitte);//Teil array1
		mergeSort(zahlen, mitte+1,rechts);//Teil array2
		merge(zahlen, links, mitte, rechts);//mischen
	}
}
private static void merge(int[] zahlen, int links, int mitte, int rechts) {
	// TODO Auto-generated method stub
	int[] hilfsArray = new int[rechts - links+1];//HilfsArray zum mischen
	int linksindex = links;//Index der linken Teilliste
	int rechtsindex = mitte;  +1;//Index der rechten Teilleiste
	int i=0;//Index des naechsten Elements
	int j=0;//zaehlindex
	int[] hilfArray;
	while((linksindex <= mitte) && (rechtsindex <= rechts)){//in beiden Teilarrays sind noch Elemente
		if(zahlen[linksindex] <= zahlen[rechtsindex]){
			hilfArray[i]=zahlen[linksindex];
			linksindex++;
		}else{
			hilfArray[i]= zahlen[rechtsindex];
			rechtsindex++;
		}
		i++;
	}
	if(linksindex > mitte){//linke Teilleiste leer --> rechte Teilliste kann uebernommen werden
		for(j=0;rechtsindex+j <= rechts;j++){
			hilfArray[i+j]= zahlen[rechtsindex+j];
		}
	}else{//rechte Teilliste leer --> linke Teilleiste uebernehmen
		for(j=0; linksindex+j<=mitte;j++){
			hilfArray[i+j]=zahlen[linksindex+j];
		}
	}
	for(j=0; j < hilfArray.length;j++){
		zahlen[links+j] = hilfArray[j];
	}}}
