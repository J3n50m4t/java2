import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
public class LinkedList {
	public static void main(String[] args) {
		List<String> l = new LinkedList<String>();
		//LinkedList kann durch andere Listentypen ersetzt werden
		//String kann auch durch Object ersetzt werden oder durch einen anderen spezifischen Datentyp, um eben nur diesen zu "erlauben"
		l.add("Bier");
		l.add("Sekt");
		l.add("Rum");
		//l.add(1);
		String element1 = l.get(0);
		//l.get(Wert);
		//Collections.sort(l); um die Liste nach Kriterien zu sortieren
		System.out.println(l);
	}}
